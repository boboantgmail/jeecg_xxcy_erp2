package com.jeecg.easyui_demo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.jeecg.easyui_demo.JsonSpecialStringsTools;
import com.jeecg.easyui_demo.entity.EasyUIProducts;
import com.jeecg.easyui_demo.entity.EasyUIProductsPojo;
import com.jeecg.easyui_demo.service.EasyUIProductTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class DatagridController {
    private static final Logger logger = LoggerFactory.getLogger(DatagridController.class);

    @Autowired
    private EasyUIProductTools easyUIProductTools;

    @RequestMapping(value="/datagrid_pojo.do",produces="application/json;charset=UTF-8")
    public String datagrid_pojo_test(){

       String rtn ="";
       try {
           rtn=new ObjectMapper().writeValueAsString(EasyUIProductTools.takeProeucctspojo());
       }catch (JsonProcessingException e){

           e.printStackTrace();
       }

        return rtn;
    }

    @RequestMapping(value="/datagrid.do",produces="application/json;charset=UTF-8")
    //@ResponseBody datagrid_pojo
    public Object datagrid() {
        EasyUIProducts euips = new EasyUIProducts();
        EasyUIProductTools.takeProeuccts(euips);

        String rtn =euips.toString();

        return rtn;
    }
    @RequestMapping(value="/datagrid_easyui.do",produces="application/json;charset=UTF-8")
    //@ResponseBody
    public String datagridFromFile(){

        String rtn =easyUIProductTools.getData().toString();

        return rtn;

    }




    @RequestMapping(value="/datagrid_dyn.do",produces="application/json;charset=UTF-8")
    public String datagrid_pojo_dyn(){

        String rtn ="";
        try {
            rtn=new ObjectMapper().writeValueAsString(EasyUIProductTools.takeProeucctspojo());
        }catch (JsonProcessingException e){

            e.printStackTrace();
        }
        logger.info("------------------>>>>>"+rtn);
        return rtn;
    }
}
