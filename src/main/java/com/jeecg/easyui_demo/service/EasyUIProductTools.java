package com.jeecg.easyui_demo.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jeecg.easyui_demo.entity.EasyUIProduct;
import com.jeecg.easyui_demo.entity.EasyUIProducts;
import com.jeecg.easyui_demo.entity.EasyUIProductsPojo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
@Service
public class EasyUIProductTools {
    @Value(value="classpath:/demo_easyui/datagrid_data0.json")

    private Resource data;

    public String getData(){
        try {
            File file = data.getFile();
            String jsonData = this.jsonRead(file);

            return jsonData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
     *     读取文件类容为字符串
     * @param file
     * @return
     */
    private String jsonRead(File file){
        Scanner scanner = null;
        StringBuilder buffer = new StringBuilder();
        try {
            scanner = new Scanner(file, "utf-8");
            while (scanner.hasNextLine()) {
                String aa=scanner.nextLine();
                buffer.append(aa);
                System.out.println(aa);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
        String rtn= buffer.toString();
//        System.out.println("==========================");
//        System.out.println(rtn);
        return rtn;
    }












    public static void main(String[] args) {



        try {
            System.out.println(new ObjectMapper().writeValueAsString(takeProeucctspojo()));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    public static void takeProeuccts(EasyUIProducts euips) {

        EasyUIProduct eup1 = new EasyUIProduct();
        EasyUIProduct eup2 = new EasyUIProduct();
        euips.getEasyUIProducts().add(eup1);
        euips.getEasyUIProducts().add(eup2);

        eup1.setProductid("FI-SW-01");
        eup1.setProductname("Koi");
        eup1.setUnitcost("10.00");
        eup1.setStatus("P");
        eup1.setListprice("36.50");
        eup1.setAttr1("Large");
        eup1.setItemid("EST-1");


        eup2.setProductid("K9-DL-01");
        eup2.setProductname("Dalmation");
        eup2.setUnitcost("12.00");
        eup2.setStatus("P");
        eup2.setListprice("18.50");
        eup2.setAttr1("Spotted Adult Female");
        eup2.setItemid("EST-10");

    }


    public static EasyUIProductsPojo  takeProeucctspojo() {
        EasyUIProduct eup1 = new EasyUIProduct();

        eup1.setProductid("FI-SW-01");
        eup1.setProductname("Koi");
        eup1.setUnitcost("10.00");
        eup1.setStatus("P");
        eup1.setListprice("36.50");
        eup1.setAttr1("Large");
        eup1.setItemid("EST-1");

        EasyUIProduct eup2 = new EasyUIProduct();
        eup2.setProductid("K9-DL-01");
        eup2.setProductname("Dalmation");
        eup2.setUnitcost("12.00");
        eup2.setStatus("P");
        eup2.setListprice("18.50");
        eup2.setAttr1("Spotted Adult Female");
        eup2.setItemid("EST-10");

        List<EasyUIProduct> rows=new ArrayList<>();


        rows.add(eup1);
        rows.add(eup2);
        return  new EasyUIProductsPojo(rows);



    }

}
