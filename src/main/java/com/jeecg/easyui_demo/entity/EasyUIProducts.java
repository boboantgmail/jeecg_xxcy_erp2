package com.jeecg.easyui_demo.entity;


import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EasyUIProducts {
    List<EasyUIProduct> easyUIProducts=new ArrayList<>();

    public List<EasyUIProduct> getEasyUIProducts() {
        return easyUIProducts;
    }

    public void setEasyUIProducts(List<EasyUIProduct> easyUIProducts) {
        this.easyUIProducts = easyUIProducts;
    }

    @Override
    public String toString() {
        StringBuffer rtnb=new StringBuffer("{");
        StringBuffer rtne=new StringBuffer("]}");
        StringBuffer rtn=new StringBuffer();


        for(int i=0;i<this.easyUIProducts.size();i++) {
            rtn.append(easyUIProducts.get(i).toString());
            if(i<this.easyUIProducts.size()-1)
            {
                rtn.append(",");
            }
        }
        rtnb.append(" \"total\":").append(this.easyUIProducts.size()).append(",\"rows\":[");

        return rtnb.append(rtn.toString()).append(rtne.toString()).toString();
    }
}
