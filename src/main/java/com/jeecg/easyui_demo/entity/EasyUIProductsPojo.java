package com.jeecg.easyui_demo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class EasyUIProductsPojo {
    @JsonProperty("rows")
    List<EasyUIProduct> rows=new ArrayList<>();
    @JsonProperty("total")
    int total=0;
    private EasyUIProductsPojo(){

    }
    public EasyUIProductsPojo(List<EasyUIProduct> rr){
        this.rows=rr;
        total=rows.size();
    }
}
