package com.jeecg.easyui_demo.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EasyUIProduct {
    private String productid;
    private String productname;
    private String unitcost;
    private String status;
    private String listprice;
    private String attr1;
    private String itemid;

    //private  String vtitle= `{total":1,"rows:[`;
    @Override
    public String toString() {

        return "{" +
                "\"productid\":"    + "\"" +productid    +  "\"" +

                ",\"productname\":" + "\"" + productname +  "\"" +
                ",\"unitcost\":"    + "\"" + unitcost    +  "\"" +
                ",\"status\":"      + "\"" + status      +  "\"" +
                ",\"listprice\":"   + "\"" + listprice   +  "\"" +
                ",\"attr1\":"       + "\"" + attr1       +  "\"" +
                ",\"itemid\":"      + "\"" + itemid      +  "\"" +
                '}';
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getUnitcost() {
        return unitcost;
    }

    public void setUnitcost(String unitcost) {
        this.unitcost = unitcost;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getListprice() {
        return listprice;
    }

    public void setListprice(String listprice) {
        this.listprice = listprice;
    }

    public String getAttr1() {
        return attr1;
    }

    public void setAttr1(String attr1) {
        this.attr1 = attr1;
    }

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }
}
