package com.jeecg.demo.controller;

import com.jeecg.demo.dao.JeecgMinidaoDao;
import com.jeecg.demo.entity.JeecgDemoEntity;
import org.jeecgframework.core.common.controller.BaseController;
import org.jeecgframework.core.common.exception.BusinessException;
import org.jeecgframework.core.common.hibernate.qbc.CriteriaQuery;
import org.jeecgframework.core.common.model.json.DataGrid;
import org.jeecgframework.tag.core.easyui.TagUtil;
import org.jeecgframework.web.system.service.MutiLangServiceI;
import org.jeecgframework.web.system.service.SystemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Validator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/jeecgXxcyListController")
public class JeecgXxcyListController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(JeecgListDemoController.class);
    @Autowired
    private SystemService systemService;

    @Autowired
    private Validator validator;

    @Autowired
    private JeecgMinidaoDao jeecgMinidaoDao;

    @Autowired
    private MutiLangServiceI mutiLangService;
    /**
     * 行编辑列表
     */
    @RequestMapping(params = "rowListPartyType")
    public ModelAndView rowListDemo(HttpServletRequest request) {
        return new ModelAndView("com/jeecg/demo/list_rowedtior");
    }
    @RequestMapping(params = "datagrid")
    public void datagrid(JeecgDemoEntity jeecgDemo, HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
        CriteriaQuery cq = new CriteriaQuery(JeecgDemoEntity.class, dataGrid);
        //查询条件组装器
        org.jeecgframework.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, jeecgDemo, request.getParameterMap());
        try{
            //自定义追加查询条件
        }catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
        cq.add();
       // this.jeecgDemoService.getDataGridReturn(cq, true);
        //String total_salary = String.valueOf(jeecgMinidaoDao.getSumSalary());
        /*
         * 说明：格式为 字段名:值(可选，不写该值时为分页数据的合计) 多个合计 以 , 分割
         */
        //dataGrid.setFooter("salary:"+(total_salary.equalsIgnoreCase("null")?"0.0":total_salary)+",age,email:合计");
        List<JeecgDemoEntity> list = dataGrid.getResults();
        Map<String, Map<String,Object>> extMap = new HashMap<String, Map<String,Object>>();
        for(JeecgDemoEntity temp:list){
            //此为针对原来的行数据，拓展的新字段
            Map m = new HashMap();
            m.put("extField",this.jeecgMinidaoDao.getOrgCode(temp.getDepId()));
            extMap.put(temp.getId(), m);
        }
//		dataGrid.setFooter("salary,age,name:合计");
        dataGrid.setFooter("[{'salary':'','age':'','name':'合计'}]");
        TagUtil.datagrid(response, dataGrid, extMap);
    }
}
