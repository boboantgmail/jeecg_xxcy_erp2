package com.jeecg.tk.mybatis.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jeecg.easyui_demo.service.EasyUIProductTools;
import com.jeecg.tk.mybatis.web.model.SysDict_S;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import com.jeecg.tk.mybatis.web.model.SysDict;
import com.jeecg.tk.mybatis.web.service.DictService;

import java.util.List;

/**
 * @author liuzh
 */
@RestController
public class DictController {
    private static final Logger logger = LoggerFactory.getLogger(DictController.class);
    @Autowired
    private DictService dictService;

    @RequestMapping(value="datagrid_mybatis_add.do")
    public String updateSysdic(SysDict sysDict) {
    	dictService.saveOrUpdate(sysDict);
    	return "jquery-easyui-1.6.11/demo/datagrid/basic_mybatis1.html";
    }
    
    @RequestMapping(value="datagrid_mybatis.do",produces="application/json;charset=UTF-8")
    public String datagrid_mysql_sysdic(){
        SysDict sysDict= new SysDict();
        sysDict.setCode("季度");
        Integer offset=0;
        Integer limit=100;


        List<SysDict> dicts = dictService.findBySysDict(sysDict, offset, limit);
        SysDict_S ss= new SysDict_S(dicts);
        String rtn ="";
        try {
            rtn=new ObjectMapper().writeValueAsString(ss);
        }catch (JsonProcessingException e){

            e.printStackTrace();
        }
        logger.info("------------>dyn mysql:rtn:"+rtn);
        return rtn;
    }

    @RequestMapping(value="/datagrid_mybatis2.do",produces="application/json;charset=UTF-8")
    public String datagrid_mysql_sysdic2(SysDict sysDict, Integer offset, Integer limit){
        logger.info("@@@@@@@@@@@@@sysDict:"+sysDict+",offset:"+offset+",limit:"+limit);
       try {
           logger.info(org.apache.commons.beanutils.BeanUtils.describe(sysDict).toString());
       }
       catch (Exception ex){
           ex.printStackTrace();
       }
        List<SysDict> dicts = dictService.findBySysDict(sysDict, offset, limit);
        SysDict_S ss= new SysDict_S(dicts);
        String rtn ="";
        try {
            rtn=new ObjectMapper().writeValueAsString(ss);
        }catch (JsonProcessingException e){

            e.printStackTrace();
        }
        logger.info("------------>dyn mysql:rtn:"+rtn);
        return rtn;
    }


    /**
     * 显示字典数据列表
     * 
     * @param sysDict
     * @param offset
     * @param limit
     * @return
     */
    @RequestMapping
    public ModelAndView dicts(SysDict sysDict, Integer offset, Integer limit) {
        ModelAndView mv = new ModelAndView("dicts");
        List<SysDict> dicts = dictService.findBySysDict(sysDict, offset, limit);
        mv.addObject("dicts", dicts);
        return mv;
    }

    /**
     * 新增或修改字典信息页面，使用 get 跳转到页面
     * 
     * @param id
     * @return
     */
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public ModelAndView add(Long id) {
        ModelAndView mv = new ModelAndView("dict_add");
        SysDict sysDict;
        if(id == null){
        	//如果 id 不存在，就是新增数据，创建一个空对象即可
            sysDict = new SysDict();
        } else {
        	//如果 id 存在，就是修改数据，把原有的数据查询出来
            sysDict = dictService.findById(id);
        }
        mv.addObject("model", sysDict);
        return mv;
    }

    /**
     * 新增或修改字典信息，通过表单 post 提交数据
     * 
     * @param sysDict
     * @return
     */
    @RequestMapping(value = "add", method = RequestMethod.POST)
    public ModelAndView save(SysDict sysDict) {
        ModelAndView mv = new ModelAndView();
        try {
            dictService.saveOrUpdate(sysDict);
            mv.setViewName("redirect:/dicts");
        } catch (Exception e){
            mv.setViewName("dict_add");
            mv.addObject("msg", e.getMessage());
            mv.addObject("model", sysDict);
        }
        return mv;
    }

    /**
     * 通过 id 删除字典信息
     * 
     * @param id
     * @return
     */
    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public ModelMap delete(@RequestParam Long id) {
        ModelMap modelMap = new ModelMap();
        try {
            boolean success = dictService.deleteById(id);
            modelMap.put("success", success);
        } catch (Exception e) {
            modelMap.put("success", false);
            modelMap.put("msg", e.getMessage());
        }
        return modelMap;
    }

}
