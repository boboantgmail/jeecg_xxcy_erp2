package com.jeecg.tk.mybatis.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class SysDict_S {
    @JsonProperty("rows")
    private List<SysDict> dicts=new ArrayList<>();
    @JsonProperty("total")
    int total=0;
    public SysDict_S(List<SysDict> dicts){
        this.dicts=dicts;
        this.total=dicts.size();

    }

    private SysDict_S(){

    }
}
